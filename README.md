# Linux.Day.2016

#### Descripción del Evento.
**Linux®** es un sistema operativo libre y de código abierto
que comenzó como un pequeño proyecto por Linus Torvalds en 1991.
En la actualidad se utiliza en todos los equipos que puedas
imaginar, desde sistemas embebidos en los submarinos nucleares,
también en los equipos de Wall Street, la Bolsa de  Valores de
Londres, El Gran Colisionador de Hadrones y los superordenadores
de la NASA.

**Linux Day:** es un dia de celebración que organizan muchas
comunidades a nivel mundial en agradecimiento a Linux.

Este año se realizara un cambio, **Linux Week** celebra su 27 aniversario el 27 de octubre de 2018.

En Panamá se estará celebrando:
- Fecha: 24 al 25 de octubre de 2018 [(Agenda)](https://github.com/floss-pa/Agenda_Anual)
- Lugar: Universidad Interamericana de Panamá, Universidad de Panamá, Universidad Interamericana de Panamá.

Los estudiantes y profesores de la Universidades
junto a  miembros de Floss-Pa estaran coordinando la realización del evento:

- Abdel Martínez
- José Reyes.
- Shelsy Chanis.
- David López.


Talleres y eventos organizado por el Grupo Universitario de Floss-Pa UIP. Mantente informado de más talleres y eventos en nuestra página web <https://floss-pa.net> o Twitter(@flosspa) y Facebook <https://fb.com/flosspa>
